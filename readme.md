```
USAGE
       linkStatus [address]

RESULTS
       If no [address] is provided, and there's Internet connection: online

       If there's no Internet: offline

       If the address is directly or indirectly reachable: reachable

       If the address is unreachable: unreachable
```
